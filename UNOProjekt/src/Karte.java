
public class Karte {

	private String farbe;
	private int zahl;
	private int wert;
	private String spez;

	public Karte(String farbe, int zahl, int wert) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.wert = wert;
	}

	public Karte(String farbe, int zahl, int wert, String spez) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.spez = spez;
		this.wert = wert;
	}

	public String getFarbe() {
		return farbe;
	}

	public int getZahl() {
		return zahl;
	}

	public int getWert() {
		return wert;
	}

	public String getSpez() {
		return spez;
	}

	public String toString() {

		return String.format(" %s %d ", this.farbe, this.zahl);
	}

	public Boolean passtdrauf(Karte Ablagenkarte) {
		if (Ablagenkarte.zahl == this.zahl) {
			return true;
			
		} else if (Ablagenkarte.farbe == this.farbe) {
			return true;
		}

		else if (this.zahl == 100 && this.farbe == "schwarz") { // Spezialkarten werden getestet
			return true;
		}
		
		else if ( Ablagenkarte.farbe == "schwarz") {
			return true;
		}

		return false;
	}

}
