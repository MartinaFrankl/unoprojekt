import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class TestmainSQL {
	
	
	private static final String CREATETABLE = "CREATE TABLE Sessions (Number int, Spieler varchar(100) NOT NULL, Sessionnumber int not null, Wert int NOT NULL, CONSTRAINT PK_Sessions PRIMARY KEY (Number))";
	private static final String INSERT_TEMPLATE= "INSERT INTO Sessions (Spieler, Sessionnumber, Wert) VALUES ('%1s', %2d, %3d );";
	private static final String SELECT_BYPLAYERANDSESSION = "SELECT Spieler, SUM(Wert) AS Wert FROM Sessions WHERE Spieler = '%1s' AND Sessionnumber = %2d;";

	public static void main(String[] args) {
		
		try{
			SqliteClient client = new SqliteClient("demo.sqlite");
			if (client.tableExists("Sessions")){ // wenn die Tabelle bereits existiert, dann l�schen
				client.executeStatement("DROP TABLE Sessions;");
			}
			client.executeStatement(CREATETABLE); // erstelle Tabelle
			
			client.executeStatement(String.format(INSERT_TEMPLATE, "Anita", 1, 50));
			client.executeStatement(String.format(INSERT_TEMPLATE, "Hans", 1, 0));
			client.executeStatement(String.format(INSERT_TEMPLATE, "Anita", 1, 20));
			client.executeStatement(String.format(INSERT_TEMPLATE, "Hans", 1, 100));
			
			ArrayList<HashMap<String, String>> results = client.executeQuery(String.format(SELECT_BYPLAYERANDSESSION, "Anita", 1));
			
			for (HashMap<String, String> map : results) {
				System.out.println(map.get("Spieler") + " hat derzeit:  " + map.get("Wert") + " Punkte");
			}
		}catch (SQLException ex) {
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		
		
		

	}

}
