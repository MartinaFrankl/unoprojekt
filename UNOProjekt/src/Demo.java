

import java.sql.*;
import java.util.*;
import java.util.Map.Entry;

public class Demo {

	
	public static SqliteClient client;

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		Scanner s = new Scanner(System.in);

		Spiel game1 = new Spiel();

		ArrayList<Integer> Summe = new ArrayList<>();
		HashMap<Integer, Spieler> Loser = new HashMap<>();
		Boolean winner = false;
		Boolean Kartepasstdrauf = false;
		Karte ablage1 = game1.austeilenUndAblageInitialisieren();
		ArrayList<Karte> Stapel = new ArrayList<Karte>();
		Boolean isUno = false;
		int Sessionnumber = 0;
		Stapel = game1.getStapel();

		menue();

		int wievieleSpieler = HowMany(" Wieviele echte Spieler werden teilnehmen?", scanner);

		// erstellen von n Spielern mit Scanner-Namenseingabe
		for (int i = 1; i <= wievieleSpieler; i++) {

			game1.mitspielen(new Spieler(getEingabe(" Spieler " + i + ":" + " Bitte geben Sie Ihren Namen ein: ", s)));
		}

		int bots = 4 - wievieleSpieler;

		// noch einmal alle Namen und die Anzahl der Bots wiedergeben
		System.out.println(game1.getMitspieler() + " werden heute zusammen mit " + bots + " Bot(s) spielen\n");

		// Datenbankkram
//		DBHelper database = new DBHelper();
//		database.init();
//		database.delete(); // muss am Ende weg
		Database DB = new Database();
		try {
			 client = new SqliteClient("UnoDatenbank.sqlite");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DB.erstelleTabelle(client);

		gameloop: 
			while (winner != true) {

			Sessionnumber++; // wird mit jeder Session hochgez�hlt

			Roundloop: 
				for (int i = 0; i < game1.getMitspieler().size(); i++) {
				int Kartenwert = 0;
				System.out.println();
				System.out.println("----------------> Karte auf Ablage: " + ablage1);
				System.out.println();
				Spieler Temp = game1.getMitspieler().get(i);
				System.out.println(
						"--------------------Spieler: " + Temp.toString() + " ist dran----------------------\n");
				Temp.Blattprint();

				Kartepasstdrauf = false;

				Kartepasstdraufloop: 
					while (!Kartepasstdrauf) {

					Karte ka = Temp.kartenausw�hlen(scanner);
					System.out.println(" Sie haben { " + ka.getFarbe() + ", " + ka.getZahl() + " } gew�hlt");
					System.out.println();

					if (ka.passtdrauf(ablage1)) {
//					ArrayList<Karte> Blatt = Temp.neuesBlatt(ka);
						Temp.neuesBlatt(ka);
						System.out.println(" Karte passt");
						Kartepasstdrauf = true;
						Stapel.add(ablage1);
						ablage1 = ka;

						isUno = Temp.getUno(s, isUno);

						if (isUno) {

							for (Spieler sp : game1.getMitspieler()) {

								for (Karte k : sp.getBlatt()) {
									Kartenwert = Kartenwert + k.getWert();
								}

								Loser.put(Kartenwert, sp);
								System.out.println(Kartenwert);

//								database.InsertWert(sp.getName(), Kartenwert);
//								database.WertGesamt(sp.getName(), Kartenwert);

								DB.Insert(sp.getName(), Kartenwert, Sessionnumber, client);
							}
							Integer min = 500;
							Spieler sp1 = new Spieler("Testdummy");

							for (Entry<Integer, Spieler> Entry : Loser.entrySet()) {

								if (Entry.getKey() < min) {
									min = Entry.getKey();
									sp1 = Entry.getValue();
								}
							}

							System.out.println(" Der Gewinner ist " + sp1.getName());

							for (Entry<Integer, Spieler> Entry : Loser.entrySet()) {

								if (Entry.getKey() > 500) {
									System.out.println(" Der Verlierer ist " + Entry.getValue().getName());
									winner = true;
									System.exit(0);
								}
							}

							break Roundloop;
						}
					} else {
						System.out.println("Karte passt nicht");
					}
					if (Abfrage(s, "Wollen Sie einen �berblick �ber den Punktestand?")) {
						try {
							ArrayList<HashMap<String, String>> results = DB.ZwischenstandsAbfrage(client);
						

								for (HashMap<String, String> entry : results) {

									for (HashMap.Entry<String, String> e : entry.entrySet()) {
										System.out.println("Gewichtsklasse: " + e.getKey() + " Enten: " + e.getValue());
									}
									
									
								}
//							

						} catch (SQLException e) {
							e.printStackTrace();
						}
					}

				}
				System.out.println("-------------------N�chster Spieler ist drann----------------------------");

			}

		}

//		while (spiel.spielzug()); //braucht keinen Schleifenk�rper? ruft immer wieder den Spielzug auf
	}

	// Namenseingabe
	public static String getEingabe(String aufforderung, Scanner scanner) {

		String name = "";
		while (name.length() == 0) {
			System.out.print(aufforderung);
			name = scanner.nextLine();

		}
		return name;
	}

	public static void menue() {
		String hello = "\n Hallo und Herzlich Willkommen bei UNO! " + "\n    erstellt von Martina und Monika";
		hello += "\n-----------------------------------------------------------------------";
		System.out.println(hello);
	}

	public static int HowMany(String aufforderung, Scanner scanner) {
		int wievieleSpieler = 0;
		while (wievieleSpieler == 0) {
			System.out.println(aufforderung);
			wievieleSpieler = scanner.nextInt();

		}
		return wievieleSpieler;
	}

	public static Boolean Abfrage(Scanner s, String aufforderung) {

		Boolean Ausgabe = false;

		String willAusgabe = "";

		while (willAusgabe.length() == 0) {
			System.out.println(aufforderung);
			willAusgabe = s.nextLine();
		}
		if (willAusgabe.toLowerCase().equals("ja")) {
			Ausgabe = true;
			return Ausgabe;
		} else {
			return Ausgabe;
		}

	}

}
