package at.campus02.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel {

	private ArrayList<UnoKarte> kartenStapel = new ArrayList<UnoKarte>();
	private ArrayList<UnoKarte> ablage = new ArrayList<UnoKarte>();
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();

	public UnoSpiel() {
		for (int kartenWert = 0; kartenWert < 10; kartenWert++) {
			kartenStapel.add(new UnoKarte("rot", kartenWert));
			kartenStapel.add(new UnoKarte("blau", kartenWert));
			kartenStapel.add(new UnoKarte("gr�n", kartenWert));
			kartenStapel.add(new UnoKarte("gelb", kartenWert));
		}

		mischen();
	}

	public void mischen() {
		Collections.shuffle(kartenStapel);
	}

	public void mitspielen(Spieler neuerSpieler) {
		mitspieler.add(neuerSpieler);
	}

	public void austeilen() {
		for (int counter = 0; counter < 7; counter++) {
			for (Spieler spieler : mitspieler) {
				UnoKarte abgehobeneKarte = kartenStapel.remove(0);
				spieler.aufnehmen(abgehobeneKarte);
			}
		}

		UnoKarte aufgedeckteKarte = kartenStapel.remove(0);
		ablage.add(aufgedeckteKarte);
	}

	public boolean spielzug() {
		Spieler aktuellerSpieler = mitspieler.remove(0);
		System.out.printf("%s: ", aktuellerSpieler.getName());

		UnoKarte obersteKarte = ablage.get(0);// bleibt am stapel
		UnoKarte karte = aktuellerSpieler.ausw�hlen(obersteKarte);

		if (karte != null) // eine passende karte
		{
			ablage.add(0, karte); // an die 1.stelle einf�gen
			System.out.printf("legt %s ab \n", karte);
		} else {
			System.out.printf("muss ziehen, ");
			UnoKarte abgehobeneKarte = abheben();

			if (abgehobeneKarte.passtDrauf(obersteKarte)) {
				ablage.add(0, abgehobeneKarte);
				System.out.printf(" legt %s ab \n", abgehobeneKarte);
			} else {
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
				System.out.printf("%s muss Karte aufnehmen \n",
						aktuellerSpieler.getName());
			}
		}

		if (!aktuellerSpieler.isFertig())
			mitspieler.add(aktuellerSpieler);

		return mitspieler.size() > 1;
	}

	private UnoKarte abheben() {

		if (kartenStapel.isEmpty()) {
			System.out.println("\n ...Umsortieren...");
			UnoKarte obersteKarte = ablage.remove(0);
			kartenStapel.addAll(ablage); // Alle Eintr�ge kopierem
			mischen();

			ablage.clear(); // ablageStapel leeren
			ablage.add(obersteKarte); // alto oberste Karte einf�gen

			return kartenStapel.remove(0);

		} else {
			UnoKarte abgehobeneKarte = kartenStapel.remove(0);
			return abgehobeneKarte;
		}
	}

	public String toString() {
		if (ablage.isEmpty()) {
			return String.format("%s am stapel sind %d Karten", mitspieler,
					kartenStapel.size());
		}

		return String.format("%s am stapel sind %d Karten, %s", mitspieler,
				kartenStapel.size(), ablage.get(0));
	}

}
