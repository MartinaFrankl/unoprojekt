package at.campus02.uno;

import java.util.HashSet;

public class Spieler {
	private String name;

	private HashSet<UnoKarte> handkarten = new HashSet<UnoKarte>();

	public Spieler(String name) {
		this.name = name;
	}

	public void aufnehmen(UnoKarte neueKarte) {
		handkarten.add(neueKarte);
	}

	public UnoKarte ausw�hlen(UnoKarte obersteKarte) {
		for (UnoKarte vergleichsKarte : handkarten) {
			if (vergleichsKarte.passtDrauf(obersteKarte)) {
				handkarten.remove(vergleichsKarte);
				if (handkarten.size() == 1)
					System.out.printf("%s UNO!! ", name);
				
				if (isFertig())
					System.out.printf("%s UNO UNO!! ", name);
				
				return vergleichsKarte;
			}
		}

		return null;
	}

	public boolean isFertig() {
		return handkarten.isEmpty();
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return String.format("%s hat %d Karten", name, handkarten.size());
	}
}
