package at.campus02.uno;

public class UnoKarte {

	private int zahl;
	private String farbe;

	public UnoKarte(String farbe, int zahl) {
		this.farbe = farbe;
		this.zahl = zahl;
	}

	public int getZahl() {
		return zahl;
	}
	
	public String getFarbe() {
		return farbe;
	}

	public boolean passtDrauf(UnoKarte andereKarte) {
		if (this.farbe == andereKarte.getFarbe())
			return true;

		if (this.zahl == andereKarte.getZahl())
			return true;

		return false;
	}

	public String toString() {
		return String.format("(%d,%s)", zahl, farbe);
	}

}
