import at.campus02.uno.Spieler;
import at.campus02.uno.UnoSpiel;

public class UnoApp {

	public static void main(String[] args) {
		UnoSpiel spiel = new UnoSpiel();
		System.out.println(spiel);

		spiel.mitspielen(new Spieler("Gerhard"));
		spiel.mitspielen(new Spieler("Andrea"));
		spiel.mitspielen(new Spieler("Hansi"));
		spiel.mitspielen(new Spieler("Mathias"));
		spiel.mitspielen(new Spieler("Gabi"));
		System.out.println(spiel);

		spiel.austeilen();
		System.out.println(spiel);

		while(spiel.spielzug());
		
		System.out.println(spiel);

	}

}
