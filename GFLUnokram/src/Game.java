import java.util.Scanner;

import at.campus02.uno.Spieler;
import at.campus02.uno.UnoSpiel;

public class Game {

	public static void main(String[] args) {
		
		
		Scanner scanner = new Scanner(System.in);
		
		
		
		UnoSpiel spiel = new UnoSpiel();
		
		System.out.println(spiel);
		
		//erstellen von 2 Spielern mit Scanner-Namenseingabe
		spiel.mitspielen(new Spieler(getEingabe("Bitte Name eingeben: ", scanner)));
		spiel.mitspielen(new Spieler(getEingabe("Bitte Name eingeben: ", scanner)));

		System.out.println(spiel);
		
		spiel.austeilen();
		
		System.out.println(spiel);
		
		while (spiel.spielzug()); //braucht keinen Schleifenkörper? ruft immer wieder den Spielzug auf
	}
	
	
	
	// Namenseingabe
	public static String getEingabe(String aufforderung, Scanner scanner) {

		String name = "";
		while (name.isEmpty()) {
			System.out.print(aufforderung);

			name = scanner.nextLine();
		}
		return name;
	}

}
