package at.campus02.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel {

	private ArrayList<UnoKarte> Kartenstapel = new ArrayList<UnoKarte>();
	private ArrayList<UnoKarte> Ablage = new ArrayList<UnoKarte>();
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();

	public UnoSpiel() {
		for (int i = 0; i < 10; i++) {
//			UnoKarte neueKarte = new UnoKarte("rot", i);
//			abhebeStapel.add(i);
			Kartenstapel.add(new UnoKarte("rot", i, i));
			Kartenstapel.add(new UnoKarte("blau", i, i));
			Kartenstapel.add(new UnoKarte("gr�n", i, i)); // Kartendeck wird bef�llt
			Kartenstapel.add(new UnoKarte("gelb", i, i));
			Kartenstapel.add(new UnoKarte("rot", i, i));
			Kartenstapel.add(new UnoKarte("blau", i, i));
			Kartenstapel.add(new UnoKarte("gr�n", i, i)); // Kartendeck wird bef�llt
			Kartenstapel.add(new UnoKarte("gelb", i, i));
		}
		
		for ( int i = 0; i < 2; i++) {
			Kartenstapel.add(new UnoKarte("blau", 0, "Retour", 20));
			Kartenstapel.add(new UnoKarte("rot", 0, "Retour", 20));
			Kartenstapel.add(new UnoKarte("gelb", 0, "Retour", 20));
			Kartenstapel.add(new UnoKarte("gr�n", 0, "Retour", 20));
		}
		
		for ( int i = 0; i < 2; i++) {
			Kartenstapel.add(new UnoKarte("blau", 0, "Plus2", 20));
			Kartenstapel.add(new UnoKarte("rot", 0, "Plus2", 20));
			Kartenstapel.add(new UnoKarte("gelb", 0, "Plus2", 20));
			Kartenstapel.add(new UnoKarte("gr�n", 0, "Plus2", 20));
		}
		
		for ( int i = 0; i < 2; i++) {
			Kartenstapel.add(new UnoKarte("blau", 0, "Aussetzen", 20));
			Kartenstapel.add(new UnoKarte("rot", 0, "Aussetzen", 20));
			Kartenstapel.add(new UnoKarte("gelb", 0, "Aussetzen", 20));
			Kartenstapel.add(new UnoKarte("gr�n", 0, "Aussetzen", 20));
		}
		for (int i = 0; i < 4; i++) {
			Kartenstapel.add(new UnoKarte("schwarz", 0, "Plus4", 50));
		}
		
		for ( int i = 0; i < 4; i++) {
			Kartenstapel.add(new UnoKarte("schwarz", 0, "Farbwahl", 50));
		}
		

		mischen(); // Methode von der Klasse auch in der Klasse benutzen
	}

	public String toString() {

		if (Ablage.isEmpty()) {
			return String.format("%s am Stapel sind %d Karten", mitspieler, Kartenstapel.size());
		}
		return String.format("%s am Stapel sind %d Karten, %s", mitspieler, Kartenstapel.size(), Ablage.get(0));
	}

	public void mischen() {
		Collections.shuffle(Kartenstapel);
	}

	public void mitspielen(Spieler neuerSpieler) { // Spieler wird zur Arraylist hinzugef�gt
		mitspieler.add(neuerSpieler);
	}

	public void austeilen() {
		for (int i = 0; i < 7; i++) { // 7 Karten werden ausgeteilt
			for (Spieler s : mitspieler) { // und zwar jedem Spieler
				UnoKarte abgehobeneKarte = Kartenstapel.remove(0);
				s.aufnehmen(abgehobeneKarte);
			}
		}
		UnoKarte aufgedeckteKarte = Kartenstapel.remove(0);
		Ablage.add(aufgedeckteKarte); // erste aufgedeckte Karte neben dem Stapel
	}

	public boolean spielzug() { // solang noch mehr wie ein Spieler drinnen ist
		Spieler aktuellerSpieler = mitspieler.remove(0);

		UnoKarte obersteKarte = Ablage.get(0); // bleibt auf dem Stapel drauf im Gegensatz zu remove;
		UnoKarte karte = aktuellerSpieler.ausw�hlen(obersteKarte);

		if (karte != null) // eine passende Karte
		{
			Ablage.add(0, karte);
			System.out.printf("%s legt %s ab \n", aktuellerSpieler.getName(), karte);
		} else {
			UnoKarte abgehobenekarte = abheben();
			System.out.printf("%s muss ziehen \n", aktuellerSpieler.getName());

			if (abgehobenekarte.kompatibel(obersteKarte)) {
				Ablage.add(0, abgehobenekarte);
				System.out.printf("%s legt %s ab \n", aktuellerSpieler.getName(), abgehobenekarte);

			} else {
				aktuellerSpieler.aufnehmen(abgehobenekarte);
				System.out.printf("%s muss Karte aufnehmen \n", aktuellerSpieler.getName());
			}
		}
		if (!aktuellerSpieler.isfertig())
			mitspieler.add(aktuellerSpieler);

		return mitspieler.size() > 1;
	}

	private UnoKarte abheben() {

		if (Kartenstapel.isEmpty()) {
			System.out.println("umsortieren...");

			UnoKarte obersteKarte = Ablage.remove(0);

			Kartenstapel.addAll(Ablage); // alle Elemente aus dem Abhebestapel in den Ablegestapel hineingeben;
											// sind aber noch immer im ablegestapel drinnen
			Ablage.clear();
			Ablage.add(obersteKarte);
			mischen();

			return Kartenstapel.remove(0);
		} else {
			UnoKarte abgehobenekarte = Kartenstapel.remove(0);
			return abgehobenekarte;
		}

	}

}
