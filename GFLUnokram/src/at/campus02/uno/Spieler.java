package at.campus02.uno;

import java.util.HashSet;

public class Spieler {
	
	private String name;
	private HashSet<UnoKarte> Blatt = new HashSet<UnoKarte>();
	
	public Spieler(String name) {
		this.name = name;
	}
	
	public void aufnehmen(UnoKarte neueKarte) {
		Blatt.add(neueKarte);
		}
	
	public String getName() {
		return name;
	}
	
	public UnoKarte ausw�hlen (UnoKarte obersteKarte) {
		for( UnoKarte u : Blatt) {
			if (u.kompatibel(obersteKarte)) {
				Blatt.remove(u);
				if(Blatt.size() == 1) { // dieser Teil m�sste umgeschrieben werden
					System.out.printf("UNO!\n", name);
				}
				if (isfertig())
				{
					System.out.printf("%s hat UNO UNO!\n", name);
				}
				return u;
			}
		}
		return null;
	}
	
	public String toString() {
		return String.format("%s hat %d Karten", name, Blatt.size());
	}
	
	public boolean isfertig() {
		if(Blatt.isEmpty()) {
			return true;
		}
		return false; //oder return handkarten.isEmpty();
	}

}
