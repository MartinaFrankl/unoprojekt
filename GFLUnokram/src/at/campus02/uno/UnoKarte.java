package at.campus02.uno;

public class UnoKarte {

	private String farbe;
	private int zahl;
	private int wert;
	private String spezial;
	
	public UnoKarte (String farbe, int zahl, int wert) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.wert = wert;
	}
	
	public UnoKarte( String farbe, int zahl, String spezial, int wert ) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.spezial = spezial;
		this.wert = wert;
	}
	
	public int getZahl() {
		return zahl;
	}
	public String getFarbe() {
		return farbe;
	}
	
	public boolean kompatibel(UnoKarte andereKarte) {
		if (zahl == andereKarte.zahl) {
			return true;
		}else if (farbe == andereKarte.farbe) {
			return true;
		}
		return false;
	}
	public String toString() {
		return String.format("(%s,%d)", farbe, zahl);
	}
}
