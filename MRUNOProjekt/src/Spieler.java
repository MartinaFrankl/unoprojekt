import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class Spieler {

	private String name;
	private ArrayList<Karte> Blatt = new ArrayList<Karte>();

	public Spieler(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return String.format("[ %S ]", name);
	}

	public void aufnehmen(Karte k) {
		Blatt.add(k);
	}

	public ArrayList<Karte> getBlatt() {
		return Blatt;
	}

	public void Blattprint() {
		int i = 0;

		System.out.println(" folgende Karten stehen zur Wahl: ");
		for (Karte k : Blatt) {

			System.out.print(" Position: " + i);
			i++;
			if (k.getFarbe() == "schwarz") {
				System.out.print(" { " + k.getFarbe() + ", " + k.getSpez() + " }");
			} else if (k.getZahl() == 10) {
				System.out.print(" { " + k.getFarbe() + ", " + k.getSpez() + " }");
			} else {

				System.out.print(" { " + k.getFarbe() + ", " + k.getZahl() + " }");
			}
			System.out.println();

		}
	}
//............................................................... Ziehen: ja/nein?
	public Boolean ziehen(Scanner s, Boolean zieht) {
		System.out.println("\nWollen Sie eine Karte ziehen?\n (Bitte geben Sie 'ja' oder 'nein' ein.)");
		String ziehen = s.nextLine();
		ziehen = ziehen.toLowerCase();
		if (ziehen.equals("ja")) {
			System.out.println("Sie wollen eine Karte ziehen\n");
			return zieht;
		} else if (ziehen.equals("nein")) {
			System.out.println("Sie wollen keine Karte ziehen\n");
		}
		return zieht;
	}
//...............................................................

	public Karte kartenauswählen(Scanner scanner) {
		System.out.println();
		System.out.println("Welche Karte wollen Sie wählen? Geben Sie dazu die Positionsnummer ein:");
		int auswahl = scanner.nextInt();

		return Blatt.get(auswahl);
	}

	public ArrayList<Karte> neuesBlatt(Karte k) {
		Blatt.remove(k);
		return Blatt;
	}

	public Boolean getUno(Scanner s, Boolean isUno) {
		System.out.println(
				"Bei UNO bitte 'uno' eintragen, bei UNO-UNO bitte 'unouno' eintragen, ansonsten irgendeine Eingabe: ");
		String getUno = s.nextLine();
		getUno = getUno.toLowerCase();
		if (getUno.equals("uno")) {
			System.out.println(" Sie haben UNO gesagt!\n");
			isUno = false;
			return isUno;
		} else if (getUno.equals("unouno")) {
			System.out.println(" Sie haben UNO-UNO gesagt!\n");
			isUno = true;
			return isUno;
		} else {
			isUno = false;
			return isUno;
		}
	}
	
	public int AktuellerKartenwert() {
		int aktuellerKartenwert = 0;
		
		for ( Karte k : Blatt ) {
			aktuellerKartenwert += k.getWert();
		}
		return aktuellerKartenwert;
	}
}
