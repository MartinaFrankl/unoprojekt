import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Spiel {

	private ArrayList<Karte> Stapel = new ArrayList<Karte>();
	private ArrayList<Karte> Ablage = new ArrayList<Karte>();

	private ArrayList<Spieler> Mitspieler = new ArrayList<Spieler>();

	public Spiel() {
		for (int i = 0; i < 10; i++) {
			Stapel.add(new Karte("rot", i, i));
			Stapel.add(new Karte("blau", i, i));
			Stapel.add(new Karte("gr�n", i, i)); // Kartendeck wird bef�llt
			Stapel.add(new Karte("gelb", i, i));
			Stapel.add(new Karte("rot", i, i));
			Stapel.add(new Karte("blau", i, i));
			Stapel.add(new Karte("gr�n", i, i)); // Kartendeck wird bef�llt
			Stapel.add(new Karte("gelb", i, i));
		}

		for (int i = 0; i < 2; i++) {
			Stapel.add(new Karte("blau", 10, 20, "Richtungswechsel"));
			Stapel.add(new Karte("rot", 10, 20, "Richtungswechsel"));
			Stapel.add(new Karte("gelb", 10, 20, "Richtungswechsel"));
			Stapel.add(new Karte("gr�n", 10, 20, "Richtungswechsel"));
		}

		for (int i = 0; i < 2; i++) {
			Stapel.add(new Karte("blau", 10, 20, "Plus2"));
			Stapel.add(new Karte("rot", 10, 20, "Plus2"));
			Stapel.add(new Karte("gelb", 10, 20, "Plus2"));
			Stapel.add(new Karte("gr�n", 10, 20, "Plus2"));
		}

		for (int i = 0; i < 2; i++) {
			Stapel.add(new Karte("blau", 10, 20, "Aussetzen"));
			Stapel.add(new Karte("rot", 10, 20, "Aussetzen"));
			Stapel.add(new Karte("gelb", 10, 20, "Aussetzen"));
			Stapel.add(new Karte("gr�n", 10, 20, "Aussetzen"));
		}
		for (int i = 0; i < 4; i++) {
			Stapel.add(new Karte("schwarz", 100, 50, "Plus4"));
		}

		for (int i = 0; i < 4; i++) {
			Stapel.add(new Karte("schwarz", 100, 50, "Farbwahl"));
		}

		mischen(); // Methode von der Klasse auch in der Klasse benutzbar
	}

	public void mischen() {
		Collections.shuffle(Stapel);
	}

	public void mitspielen(Spieler neuerSpieler) { // Spieler wird zur Arraylist hinzugef�gt
		Mitspieler.add(neuerSpieler);
	}

	public ArrayList<Spieler> getMitspieler() {
		return Mitspieler;
	}

	public ArrayList<Karte> getAblage() {
		return Ablage;
	}

	public ArrayList<Karte> getStapel() {
		return Stapel;
	}

	public Karte austeilenUndAblageInitialisieren() {
		for (int i = 0; i < 7; i++) { // 7 Karten werden ausgeteilt
			for (Spieler s : Mitspieler) { // und zwar jedem Spieler
				Karte abgehobeneKarte = Stapel.remove(0);
				s.aufnehmen(abgehobeneKarte);
			}
		}
		Karte aufgedeckteKarte = Stapel.remove(0);
		Ablage.add(aufgedeckteKarte); // erste aufgedeckte Karte neben dem Stapel
		return aufgedeckteKarte;
	}
	
//...............................................................
	public void karteZiehen(Spieler s) {
		Karte neueKarte = Stapel.remove(0);
		s.aufnehmen(neueKarte);
	}
//...............................................................
	
	public ArrayList<Karte> AblagenachSpielzug(Karte k) {

		Ablage.add(k);

		return Ablage;
	}

}
