import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Bots extends Spieler {
	
	private String name;

	private ArrayList<Karte> Blatt;
	
	private int Kartenwert;

	public Bots(String name) {
		super();
		this.name = name;
		Blatt = new ArrayList<Karte>();
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public int getKartenwert() {
		return Kartenwert;
	}


	@Override
	public void setKartenwert(int kartenwert) {
		Kartenwert = kartenwert;
	}



	@Override
	public ArrayList<Karte> getBlatt() {
		// TODO Auto-generated method stub
		return Blatt;
	}
	@Override
	public void setBlatt(ArrayList<Karte> Blatt) {
		this.Blatt = Blatt;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("[ %S ]", name);
	}
	
	@Override
	public void aufnehmen(Karte k) {
		// TODO Auto-generated method stub
		Blatt.add(k);
	}
	
	
	// Bots brauchen kein Print
	// Bots suchen die beste Karte aus aber müssen ziehen wenn sie keine karte haben, und weitergeben
	
	public void Blattprint() {
		int i = 0;

		System.out.println(" folgende Karten stehen zur Wahl: ");
		for (Karte k : Blatt) {

			System.out.print(" Position: " + i);
			i++;
			if (k.getFarbe() == "schwarz") {
				System.out.print(" { " + k.getFarbe() + ", " + k.getSpez() + " }");
			} else if (k.getZahl() == 10 || k.getZahl() == 11 || k.getZahl() == 12) {
				System.out.print(" { " + k.getFarbe() + ", " + k.getSpez() + " }");
			} else {

				System.out.print(" { " + k.getFarbe() + ", " + k.getZahl() + " }");
			}
			System.out.println();
		}
	}

	@Override
	public ArrayList<Karte> neuesBlattohneKarte(Karte k) {
		Blatt.remove(k);
		return Blatt;
	}
	
	@Override
	public Karte kartenauswählen(Karte ablage) {

		return null;
	}
	
	@Override
	public Boolean ziehen() {
		return false;
		
	}
	@Override
	public Boolean getUno(ArrayList<Karte> Handkarten) {
		Boolean isUnoUno = false;
		
		if (Handkarten.size() == 1) {
			System.out.println("UNO!");
			isUnoUno = false;
			return isUnoUno;
		}
		else if ( Handkarten.size() == 0) {
			System.out.println("UNO-UNO!");
			isUnoUno = true;
			return isUnoUno;
		}
		return isUnoUno;
	}
	
	
	@Override
	public int AktuellerKartenwert() {
		int aktuellerKartenwert = 0;

		for (Karte k : Blatt) {
			aktuellerKartenwert += k.getWert();
		}
		return aktuellerKartenwert;
	}
}
