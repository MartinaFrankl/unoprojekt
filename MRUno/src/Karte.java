
public class Karte {

	private String farbe;
	private int zahl;
	private int wert;
	private String spez;
	private boolean used = false;;

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public Karte(String farbe, int zahl, int wert) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.wert = wert;
	}

	public Karte(String farbe, int zahl, int wert, String spez) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.spez = spez;
		this.wert = wert;
	}

	public Karte() {
		// TODO Auto-generated constructor stub
	}

	public String getFarbe() {
		return farbe;
	}

	public int getZahl() {
		return zahl;
	}

	public int getWert() {
		return wert;
	}

	public String getSpez() {
		return spez;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}
	
	public boolean isUsed() {
		return used;
	}

	public String toString() {
		
		if ( this.wert != 20 && this.wert != 50) {
			
			return String.format(" %s %d ", this.farbe, this.zahl);
		}
		return String.format(" %s, %s", this.farbe, this.spez);

	}

	public Boolean passtdrauf(Karte Ablagenkarte) {
		if (Ablagenkarte.zahl == this.zahl) {
			return true;
			
		} else if (Ablagenkarte.farbe.equals(this.farbe)) {
			return true;
		}
		
		else if (Ablagenkarte.farbe == this.farbe) {
			return true;
		}

		else if (this.zahl == 100 && this.farbe == "schwarz") { // Spezialkarten werden getestet
			return true;
		}
		
		else if ( Ablagenkarte.farbe == "schwarz") {
			return true;
		}

		return false;
	}

}
