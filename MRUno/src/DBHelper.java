
import java.sql.*;

public class DBHelper {

	private static Connection con = null;
	
	public int mitzaehlen = 0;

	public void init() {
		try {

			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver"); // Treiber laden
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}

		try {

			con = DriverManager
					.getConnection("jdbc:ucanaccess://" + "C:/Users/martina.frankl/Desktop/" + "UnoDatenbank.mdb");

			System.out.println("--Datenbank verbunden--");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//Datenbank liefert wert der Spieler hinein
	
	public void InsertWert(String Spieler, int i) {
		
	
		try {
			
			
				
				PreparedStatement stmt = con.prepareStatement("INSERT INTO SpielerAktuell(Name, Wert) VALUES(?, ?)");
				
				stmt.setString(1, Spieler);
				stmt.setInt(2, i);
				stmt.executeUpdate();
				
			
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	
	
	public void delete() {
		
		Statement stmt;
		try {
			stmt = con.createStatement();
			int affectedRows = stmt
					.executeUpdate("Delete * from SpielerAktuell");
			
			int affectedrow = stmt.executeUpdate("Delete * from SpielerGesamt"); // muss am Ende weg
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void WertGesamt(String Spieler, int i) {
		try {
			
			if ( mitzaehlen < 5) {
				
				PreparedStatement stmt = con.prepareStatement("INSERT INTO SpielerGesamt(Name, Wert) VALUES(?, ?)");
				
				stmt.setString(1, Spieler);
				stmt.setInt(2, i);
				stmt.executeUpdate();
				
				mitzaehlen++;
				
			}
			else {
				
				int neuerwert = 0;
				PreparedStatement findWert = con.prepareStatement("Select Name, Wert from SpielerAktuell where Name = ?");
				
				findWert.setString(1, Spieler);
				
				
				
				ResultSet rs = findWert.executeQuery();
				
				if (rs.next()) {
					
					neuerwert = rs.getInt("Wert");
					i = neuerwert + i;
				}
				
				
				
				PreparedStatement stmt2 = con.prepareStatement("Update SpielerGesamt Set Wert = ? where Name = ?" );
				stmt2.setInt(1, i);
				stmt2.setString(2, Spieler);
				stmt2.executeUpdate();
				
			}
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	// Datenbank holt sich Wert und vergleicht mit 500
}
