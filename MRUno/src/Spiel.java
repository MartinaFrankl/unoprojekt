import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class Spiel {

	private ArrayList<Karte> Stapel = new ArrayList<Karte>();
	private ArrayList<Karte> Ablage = new ArrayList<Karte>();

	private ArrayList<Spieler> Mitspieler = new ArrayList<Spieler>();

	public Spiel() {
		for (int i = 1; i < 10; i++) {
			Stapel.add(new Karte("rot", i, i));
			Stapel.add(new Karte("blau", i, i));
			Stapel.add(new Karte("gr�n", i, i)); // Kartendeck wird bef�llt
			Stapel.add(new Karte("gelb", i, i));
			Stapel.add(new Karte("rot", i, i));
			Stapel.add(new Karte("blau", i, i));
			Stapel.add(new Karte("gr�n", i, i));
			Stapel.add(new Karte("gelb", i, i));
		}
			Stapel.add(new Karte("rot", 0, 0));
			Stapel.add(new Karte("gr�n", 0, 0));
			Stapel.add(new Karte("gelb", 0, 0));
			Stapel.add(new Karte("blau", 0, 0));
			
			

		for (int i = 0; i < 2; i++) {
			Stapel.add(new Karte("blau", 10, 20, "Richtungswechsel"));
			Stapel.add(new Karte("rot", 10, 20, "Richtungswechsel"));
			Stapel.add(new Karte("gelb", 10, 20, "Richtungswechsel"));
			Stapel.add(new Karte("gr�n", 10, 20, "Richtungswechsel"));
		}

		for (int i = 0; i < 2; i++) {
			Stapel.add(new Karte("blau", 11, 20, "Plus2"));
			Stapel.add(new Karte("rot", 11, 20, "Plus2"));
			Stapel.add(new Karte("gelb", 11, 20, "Plus2"));
			Stapel.add(new Karte("gr�n", 11, 20, "Plus2"));
		}

		for (int i = 0; i < 2; i++) {
			Stapel.add(new Karte("blau", 12, 20, "Aussetzen"));
			Stapel.add(new Karte("rot", 12, 20, "Aussetzen"));
			Stapel.add(new Karte("gelb", 12, 20, "Aussetzen"));
			Stapel.add(new Karte("gr�n", 12, 20, "Aussetzen"));
		}
		for (int i = 0; i < 4; i++) {
			Stapel.add(new Karte("schwarz", 100, 50, "Plus4"));
		}

		for (int i = 0; i < 4; i++) {
			Stapel.add(new Karte("schwarz", 100, 50, "Farbwahl"));
		}

		mischen(); // Methode von der Klasse auch in der Klasse benutzbar
	}

	public void mischen() {
		Collections.shuffle(Stapel);
	}

	public void mitspielen(Spieler neuerSpieler) { // Spieler wird zur Arraylist hinzugef�gt
		Mitspieler.add(neuerSpieler);
	}

	public ArrayList<Spieler> getMitspieler() {
		return Mitspieler;
	}

	public ArrayList<Karte> getAblage() {
		return Ablage;
	}

	public ArrayList<Karte> getStapel() {
		return Stapel;
	}

	public Karte austeilenUndAblageInitialisieren() {
		for (int i = 0; i < 7; i++) { // 7 Karten werden ausgeteilt
			for (Spieler s : Mitspieler) { // und zwar jedem Spieler
				Karte abgehobeneKarte = Stapel.remove(0);
				s.aufnehmen(abgehobeneKarte);
			}
		}
		Karte aufgedeckteKarte = Stapel.remove(0);
		Karte AlternativeKarte = new Karte("", 1, 1);
		Boolean ablageIsSpez = true;

		while (ablageIsSpez) {

			if (aufgedeckteKarte.getWert() == 20 || aufgedeckteKarte.getWert() == 50) {

				AlternativeKarte = Stapel.remove(0);
				aufgedeckteKarte = AlternativeKarte;
			} else {
				ablageIsSpez = false;
				Ablage.add(aufgedeckteKarte); // schaut so lange nach bis die aufgedeckte Karte keine Spezialkarte mehr
												// ist
				return aufgedeckteKarte;
			}

		}
		Ablage.add(aufgedeckteKarte); // erste aufgedeckte Karte neben dem Stapel
		return aufgedeckteKarte;
	}

	public ArrayList<Karte> AblagenachSpielzug(Karte k) {

		Ablage.add(k);

		return Ablage;
	}

	public void karteZiehen(Spieler s) {
		Karte neueKarte = Stapel.remove(0);
		s.aufnehmen(neueKarte);
	}

	public Boolean SpezialkarteTesten(Karte istSpezialkarte, Spieler aktuellerSpieler) {

		

		if (istSpezialkarte.getWert() == 50 || istSpezialkarte.getWert() == 20) {

			if (istSpezialkarte.getSpez().equals("Richtungswechsel")) {

				aktuellerSpieler = Mitspieler.remove(0);
				Collections.reverse(Mitspieler);
				Mitspieler.add(0, aktuellerSpieler);
				// 0 Spieler zwischenspeichern
				// reverse anwenden
				// 0 Spieler an 0 Stelle hinzuf�gen --> braucht man nur machen,damit der
				// aktuellespieler
				// --> auch wieder zur�ck an die Main geliefert wird
				return true;

			} else if (istSpezialkarte.getSpez().equals("Aussetzen")) {

				�berschreiben(aktuellerSpieler);

				return true;

			} else if (istSpezialkarte.getSpez().equals("Plus2")) {

				Spieler n�chsterSpieler = �berschreiben(aktuellerSpieler);
				this.karteZiehen(n�chsterSpieler); // bekommt im Hintergrund seine beiden Karten
				this.karteZiehen(n�chsterSpieler);

				return true;

			} else if (istSpezialkarte.getSpez().equals("Plus4")) {

				Spieler n�chsterSpieler = �berschreiben(aktuellerSpieler);

				this.karteZiehen(n�chsterSpieler);
				this.karteZiehen(n�chsterSpieler);
				this.karteZiehen(n�chsterSpieler);
				this.karteZiehen(n�chsterSpieler);

				return true;

			} else if (istSpezialkarte.getSpez().equals("Farbwahl")) {

				return true;
			}

		} else {

//			System.out.println("Keine Spezialkarte vorhanden");
			return false;
		}
		return false;
	}

	public String Farbwahl(Scanner s, Spieler player) {

		Boolean farbepasstnochnicht = true;
		String farbe = "";
		
		if (player.getClass() == echterSpieler.class) {
			
			while (farbepasstnochnicht) {
				
				System.out.println(
						" FARBWAHL: Zur Auswahl stehen rot, blau, gelb und gr�n! Bitte w�hlen Sie eine Farbe aus: ");
				
				farbe = s.nextLine();
				
				if (farbe.toLowerCase().equals("gr�n") || farbe.toLowerCase().equals("rot")
						|| farbe.toLowerCase().equals("blau") || farbe.toLowerCase().equals("gelb")) {
					
					farbepasstnochnicht = false;
					return farbe;
				}
				
			}
			return farbe;
		}
		else {
			ArrayList<String> farbpalette = new ArrayList<String>();
			farbpalette.add("gr�n");
			farbpalette.add("gelb");
			farbpalette.add("rot");
			farbpalette.add("blau");
			Collections.shuffle(farbpalette);
			
			farbe = farbpalette.get(0);
			System.out.println();
			System.out.println( player.getName() + " hat sich f�r die Farbe " + farbe + " entschieden");
			return farbe;
		}

	}

	public Spieler �berschreiben(Spieler aktuellerSpieler) {
		aktuellerSpieler = Mitspieler.remove(0);
		Mitspieler.add(Mitspieler.size(), aktuellerSpieler); // sicher nicht size()-1?
		return Mitspieler.get(0); // somit wird der aktuelle Spieler removed, und der n�chste returned.
									// welcher Spieler auch immer in die Main zur�ckgegeben wird, es wird direkt der
									// danach
									// kommende in der main als neuer Spieler festgelegt;
	}
	
	
	public Karte BotSpieltkarte(Spieler AktuellerSpieler, Karte ablage) {
		
		Karte dummy = null;
		

			for ( Karte k : AktuellerSpieler.getBlatt()) {
				if (k.passtdrauf(ablage)) {
					
					System.out.println();
					if (k.getFarbe() == "schwarz") {
						System.out.println();
						System.out.println( AktuellerSpieler.getName() + "hat { " + k.getSpez().toUpperCase() + " } gew�hlt");
					} else if (k.getWert() == 20) {
						System.out.println();
						System.out.println(AktuellerSpieler.getName() + "hat { " + k.getFarbe().toUpperCase() + ", " + k.getSpez().toUpperCase()
								+ " } gew�hlt");
					} else {
						System.out.println();
						System.out.println(AktuellerSpieler.getName() + "hat { " + k.getFarbe().toUpperCase() + ", " + k.getZahl() + " } gew�hlt");
						System.out.println();
					}
					
					AktuellerSpieler.neuesBlattohneKarte(k);
					if ( ablage.getZahl() == 100) {
						
						if ( ablage.getSpez().equals("Farbwahl") || ablage.getSpez().equals("Plus4")) {
							
							ablage.setFarbe("schwarz");
						}
					}
					ablage.setUsed(false);
					Stapel.add(ablage);
					ablage = k;
					return ablage;
				}
			}
		System.out.println();
		System.out.println("Keine passende karte gefunden, muss ziehen");
		return dummy;
		
		
	}
	
	public boolean Farbwahl(Spiel game, Karte Ablage) {
		if ( Ablage.getWert() == 50 ) {
			return true;
		}
		return false;
	}

}
