import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;


public class echterSpieler extends Spieler{
	private String name;
	private ArrayList<Karte> Blatt;
	private int Kartenwert;

	public echterSpieler(String name) {
		super();
		this.name = name;
		Blatt = new ArrayList<Karte>();
	}

	@Override
	public int getKartenwert() {
		return Kartenwert;
	}

	@Override
	public void setKartenwert(int kartenwert) {
		Kartenwert = kartenwert;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return String.format("[ %S ]", name);
	}

	@Override
	public void aufnehmen(Karte k) {
		Blatt.add(k);
	}

	@Override
	public ArrayList<Karte> getBlatt() {
		return Blatt;
	}
	@Override
	public void Blattprint() {
		int i = 0;
		
		System.out.println(" folgende Karten stehen zur Wahl: ");
		for (Karte k : Blatt) {

			System.out.print(" Position: " + i);
			i++;
			if (k.getFarbe() == "schwarz") {
				System.out.print(" { " + k.getFarbe() + ", " + k.getSpez() + " }");
			} else if (k.getZahl() == 10 || k.getZahl() == 11 || k.getZahl() == 12) {
				System.out.print(" { " + k.getFarbe() + ", " + k.getSpez() + " }");
			} else {

				System.out.print(" { " + k.getFarbe() + ", " + k.getZahl() + " }");
			}
			System.out.println();
		}
	}
	@Override
	public Karte kartenausw�hlen() {

		Boolean position = true;
		Karte dummy = new Karte();

		while (position) {
			Scanner newScanner = new Scanner(System.in);

			System.out.println();
			System.out.println("Welche Karte wollen Sie w�hlen? Geben Sie dazu die Positionsnummer ein:");
			if (newScanner.hasNextInt()) {

				
				int auswahl = newScanner.nextInt();

				if ( auswahl >= Blatt.size()|| auswahl < 0) {
					continue;
				}
				dummy = Blatt.get(auswahl);
				return dummy;
			}

		}
		return dummy;
	}
	@Override
	public ArrayList<Karte> neuesBlattohneKarte(Karte k) {
		Blatt.remove(k);
		return Blatt;
	}

	@Override
	public Boolean getUno(Scanner s1, ArrayList<Karte> Handkarten, Spieler aktuellerSpieler, Spiel game) {
		System.out.println(
				"Bei UNO bitte 'uno' eintragen, bei UNO-UNO bitte 'unouno' eintragen, ansonsten irgendeine Eingabe: ");
		String getUno = s1.nextLine();
		getUno = getUno.toLowerCase();
		// UNO
		if (getUno.equals("uno")) {
			System.out.println("*****Sie haben UNO gesagt!*****\n");

			if (Handkarten.size() == 1) {
				System.out.println("Und Sie haben auch wirklich UNO!");
			} else {
				System.out.println("Sie haben leider nicht wirklich UNO! Und m�ssen 2 Karten heben");
				game.karteZiehen(aktuellerSpieler);
				game.karteZiehen(aktuellerSpieler);
			}

			Boolean isUnoUno = false;
			return isUnoUno;
		}
		// UNOUNO
		else if (getUno.equals("unouno")) {
			System.out.println("*****Sie haben UNO-UNO gesagt!*****\n");

			if (Handkarten.size() == 0) {

				System.out.println("---> Damit haben Sie diese Runde gewonnen!");
				Boolean isUnoUno = true;
				return isUnoUno;
			} else {
				System.out.println("---> Sie haben zu fr�h UNO-UNO gesagt! und m�ssen 2 Karten heben<----");
				Boolean isUnoUno = false;
				game.karteZiehen(aktuellerSpieler);
				game.karteZiehen(aktuellerSpieler);
				return isUnoUno;
			}
		} else {
			// strafe
			if (Handkarten.size() == 1 || Handkarten.size() == 0) {
				System.out.println("Sie haben nicht UNO / UNOUNO gesagt, zur Strafe bekommen Sie 2 Karten!");
				game.karteZiehen(aktuellerSpieler);
				game.karteZiehen(aktuellerSpieler);
			}
			Boolean isUnoUno = false;
			return isUnoUno;
		}
	}
	@Override
	public Boolean ziehen() {
		Boolean zieht = false;
		
		Scanner s = new Scanner(System.in);
		System.out.println("\nWollen Sie eine Karte ziehen?\n(Bitte geben Sie 'ja' oder eine beliebige Eingabe f�r 'nein' ein)");
		String ziehen = s.nextLine();
		ziehen = ziehen.toLowerCase();
		if (ziehen.equals("ja")) {
			System.out.println("Sie wollen eine Karte ziehen\n");
			zieht = true;
			return zieht;
		} else if (ziehen.equals("nein")) {
			System.out.println("Sie wollen keine Karte ziehen\n");
		}
		return zieht;
	}

	@Override
	public int AktuellerKartenwert() {
		int aktuellerKartenwert = 0;

		for (Karte k : Blatt) {
			aktuellerKartenwert += k.getWert();
		}
		return aktuellerKartenwert;
	}
}
