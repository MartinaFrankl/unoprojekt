import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map.Entry;


import java.util.Scanner;

public class DemoMain {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		Scanner s1 = new Scanner(System.in);
		Scanner s2 = new Scanner(System.in);
		Scanner s3 = new Scanner(System.in);
		Scanner s4 = new Scanner(System.in);

//		menue();

		Spiel game = new Spiel();

		int wievieleSpieler = HowMany(" Wieviele echte Spieler werden teilnehmen? Maximal 4 sind m�glich: ");
		int bots = 4 - wievieleSpieler;

		// erstellen von n Spielern in die ArrayList mit Scanner-Namenseingabe
		for (int i = 1; i <= wievieleSpieler; i++) {

			game.mitspielen(
					new echterSpieler(getEingabe(" Spieler " + i + ":" + " Bitte geben Sie Ihren Namen ein: ", s)));

		}
		if (wievieleSpieler > 0) {

			System.out.println(game.getMitspieler() + " werden heute zusammen mit " + bots + " Bot(s) spielen\n");
		}

		for (int i = 1; i <= bots; i++) {
			game.mitspielen(new Bots("Bot" + i));
		}

		System.out.println("Spieler: " + game.getMitspieler());

		Karte ablage = null;
		Boolean Kartepasstdrauf = false;
		Boolean winner = false;
		Boolean isUnoUno = false;
		Boolean newSession = true;
		HashMap<Spieler, Integer> Loser = new HashMap<>();

		int Sessionnumber = 1;
		int summe = 0;
		ArrayList<Karte> Stapel = new ArrayList<Karte>();

		while (winner == false) {

			if (newSession == true) { // sobald eine session beginnt, wird die reihenfolge ge�ndert neu durchgemischt

				ablage = game.austeilenUndAblageInitialisieren();
				Collections.shuffle(game.getStapel());
				Stapel = game.getStapel();
				newSession = false;
				Collections.shuffle(game.getMitspieler());
			}

			Spieler aktuellerSpieler = game.getMitspieler().get(0);

			int Kartenwert = 0;
			int ziehen = 0;
			System.out.println();
			System.out.println("------------------------------------ Runde: " + Sessionnumber
					+ " ---------------------------------------");
			System.out.println();
			System.out.println("_____________________________________________________________________________________");
			System.out.println();
			System.out.println(" 			   Karte auf Ablage: " + ablage.toString() + "						");
			System.out.println("_____________________________________________________________________________________");
			System.out.println();
			System.out.println("---------------------------Spieler: " + aktuellerSpieler.toString()
					+ " ist dran------------------------------\n");

			aktuellerSpieler.Blattprint();

			Boolean willziehen = aktuellerSpieler.ziehen();
			if (willziehen == true) {
				ziehen++;
				// abheben einer Karte vom Stapel
				game.karteZiehen(aktuellerSpieler);
				aktuellerSpieler.Blattprint();
			}
				//Unterschied zwischen echtem Spieler und Bot
			if (aktuellerSpieler.getClass() == echterSpieler.class) {

				// hier findet der eigentliche Zug statt
				ablage = PasstAuswahl(game, s4, Kartepasstdrauf, aktuellerSpieler, ablage, Stapel, ziehen);

				// UNO wird gesagt oder eben nicht --> Straf-Abheben
				isUnoUno = aktuellerSpieler.getUno(s1, aktuellerSpieler.getBlatt(), aktuellerSpieler, game);
			}

			else if (aktuellerSpieler.getClass() == Bots.class) {

				Karte reserveablage = ablage;
				ablage = game.BotSpieltkarte(aktuellerSpieler, ablage);

				System.out.println();
				if (ablage == null) {
					game.karteZiehen(aktuellerSpieler);
					ablage = reserveablage;
				}

				isUnoUno = aktuellerSpieler.getUno(aktuellerSpieler.getBlatt());

				// damit es nicht ganz so schnell geht
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// wenn es wirklich einen Rundensieger gibt, wird erstmals die Datenbank angesprochen
			// und bef�llt
			if (isUnoUno == true) {
				
				winner = inDatenbankeintragen(game, Loser, winner);
				
				if ( winner == true) {
					System.out.println("**********Es gibt einen Gewinner, das Spiel ist zu Ende************");
					System.exit(0);
				}
				else {

					Sessionnumber++;
					System.out.println("Es startet eine neue Session!");
					newSession = true;
					continue;
				}
			}

			// testet die Karte auf Spezial:
			Boolean istSpezial = false;

			if (!ablage.isUsed()) {
				istSpezial = game.SpezialkarteTesten(ablage, aktuellerSpieler);
				if (istSpezial == true) {
					ablage.setUsed(true);
					
					Boolean braucheFarbwahl = game.Farbwahl(game, ablage);
					String Farbe = "";
					
					System.out.println();
					if (braucheFarbwahl == true) {
						Farbe = game.Farbwahl(s3, aktuellerSpieler);
						ablage.setFarbe(Farbe);
					}
				}
			}


			aktuellerSpieler = game.getMitspieler().remove(0); // aktueller Spieler wird aus der Liste genommen und
			game.getMitspieler().add(3, aktuellerSpieler); // hinten wieder angesetzt

			
			
			System.out.println("_______________________________Kontrolldaten:____________________________________");
			
			// Kontrollstrukturen: Spielerreihenfolge, KartenAnzahl im ganzen Spiel,
			// KartenAnzahl der einzelnen Spieler
			System.out.println();
			System.out.println("Aktuelle Spielerreihenfolge:" + game.getMitspieler());

			String handkartenanzahl = "";
			for (Spieler player : game.getMitspieler()) {
				handkartenanzahl += "[ " + player.getName() + " : " + player.getBlatt().size() + " ]";
			}

			System.out.println("Aktuelle Kartenanzahl der Spieler " + handkartenanzahl);

			int SpielerKartenAnzahl = 0;
			for (Spieler sp : game.getMitspieler()) {
				SpielerKartenAnzahl += sp.getBlatt().size();
			}

			int kartenanzahlkontrolle = game.getStapel().size() + game.getAblage().size() + SpielerKartenAnzahl;
			System.out.println("Anzahl der Karten insgesamt: " + kartenanzahlkontrolle);
			System.out.println();
		}

	}

	public static boolean inDatenbankeintragen(Spiel game, HashMap<Spieler, Integer> Loser, boolean winner) {

		// alle Spieler werden durch iteriert --> ihre Kartenwerte werden abgez�hlt und
		// in die Hashmap gespeichert
			int alterKartenwert;
			
			
		for (Spieler sp : game.getMitspieler()) {

			alterKartenwert = sp.getKartenwert();
			
			sp.setKartenwert(sp.AktuellerKartenwert() + alterKartenwert);
			
			for (Entry<Spieler, Integer> si : Loser.entrySet()) {
				
				if(si.getKey() == null) {
					Loser.put(sp, sp.getKartenwert());
				}
				else if (si.getKey() == sp) {
					
					Loser.put(sp, sp.getKartenwert());
				}
				
			}
			 //<---------------------------->!!! �berspeichert eine hashmap intelligent?
			
			if (sp.getKartenwert() >= 500) {
				
				winner = true;
				return winner;
			}
		}
		return false;

//		DB.Insert(sp.getName(), Kartenwert, Sessionnumber, client);

	}

	public static Karte PasstAuswahl(Spiel game, Scanner s4, Boolean Kartepasstdrauf, Spieler Temp, Karte ablage,
			ArrayList<Karte> Stapel, int ziehen) {

		while (!Kartepasstdrauf) {

			Karte ka = Temp.kartenausw�hlen();

			if (ka.getFarbe() == "schwarz") {
				System.out.println();
				System.out.println("Sie haben { " + ka.getSpez().toUpperCase() + " } gew�hlt");
			} else if (ka.getWert() == 20) {
				System.out.println();
				System.out.println("Sie haben { " + ka.getFarbe().toUpperCase() + ", " + ka.getSpez().toUpperCase()
						+ " } gew�hlt");
			} else {
				System.out.println();
				System.out.println("Sie haben { " + ka.getFarbe().toUpperCase() + ", " + ka.getZahl() + " } gew�hlt");
				System.out.println();
			}

			if (ka.passtdrauf(ablage)) {
				Temp.neuesBlattohneKarte(ka);
				System.out.println();
				System.out.println("--------> Karte passt");
				Kartepasstdrauf = true;
				ablage.setUsed(false);
				Stapel.add(ablage);
				ablage = ka;
				return ablage;
			} else {
				System.out.println();
				System.out.println(
						"--------> Karte passt nicht! \n_________________________________________________________________________________________\n Dr�cken Sie eine beliebige Taste, wenn Sie eine andere Karte ausw�hlen m�chten! "
								+ "\n Geben Sie 'weiter' ein, wenn Sie ihren Spielzug beenden wollen:  "
								+ "\n Geben Sie 'ziehen' ein, wenn Sie noch keine Karte gezogen haben und eine Karte ziehen wollen:");

				String weiter = s4.nextLine().toLowerCase();

				if (weiter.equals("weiter")) {
					return ablage;
				}

				else if (weiter.equals("ziehen") && ziehen == 0) {
					game.karteZiehen(Temp);
					ziehen++;
					Temp.Blattprint();
					// ziehen
				} else {
					System.out.println("Sie haben schon eine Karte gehoben");
				}
			}
		}
		return ablage;

	}

	public static String getEingabe(String aufforderung, Scanner scanner) {

		String name = "";
		while (name.length() == 0) {
			System.out.print(aufforderung);
			name = scanner.nextLine();

		}

		return name;
	}

	public static int HowMany(String aufforderung) {
		Boolean wieviele = true;
		int wievieleSpieler = 0;

		while (wieviele) {

			Scanner test = new Scanner(System.in);
			System.out.println(aufforderung);

			if (test.hasNextInt()) {

				wievieleSpieler = test.nextInt();
				if (wievieleSpieler > 4 || wievieleSpieler < 0) {
					continue;
				}
				wieviele = false;

				return wievieleSpieler;
			}

		}

		return wievieleSpieler;
	}

	public static Boolean Abfrage(Scanner s, String aufforderung) {

		Boolean Ausgabe = false;

		String willAusgabe = "";

		while (willAusgabe.length() == 0) {
			System.out.println(aufforderung);
			willAusgabe = s.nextLine();
		}
		if (willAusgabe.toLowerCase().equals("ja")) {
			Ausgabe = true;
			return Ausgabe;
		} else {
			return Ausgabe;
		}

	}

//	public static void menue() {
//
//		String uno = " .----------------.  .-----------------. .----------------. \r\n"
//				+ "| .--------------. || .--------------. || .--------------. |\r\n"
//				+ "| | _____  _____ | || | ____  _____  | || |     ____     | |\r\n"
//				+ "| ||_   _||_   _|| || ||_   \\|_   _| | || |   .'    `.   | |\r\n"
//				+ "| |  | |    | |  | || |  |   \\ | |   | || |  /  .--.  \\  | |\r\n"
//				+ "| |  | '    ' |  | || |  | |\\ \\| |   | || |  | |    | |  | |\r\n"
//				+ "| |   \\ `--' /   | || | _| |_\\   |_  | || |  \\  `--'  /  | |\r\n"
//				+ "| |    `.__.'    | || ||_____|\\____| | || |   `.____.'   | |\r\n"
//				+ "| |              | || |              | || |              | |\r\n"
//				+ "| '--------------' || '--------------' || '--------------' |\r\n"
//				+ " '----------------'  '----------------'  '----------------' ";
//
//		String hello = "\t\n	       Hallo und Herzlich Willkommen bei \t\n" + uno
//				+ "\n\n     	       erstellt von Monika und Martina";
//		hello += "\n__________________________________________________________________";
//		System.out.println(hello);
//		System.out.println();
//	}
}
