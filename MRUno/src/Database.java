import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Database {

	private static final String CREATETABLE = "CREATE TABLE Sessions (Number int, Spieler varchar(100) NOT NULL, Sessionnumber int not null, Wert int NOT NULL, CONSTRAINT PK_Sessions PRIMARY KEY (Number))";
	private static final String INSERT_TEMPLATE = "INSERT INTO Sessions (Spieler, Sessionnumber, Wert) VALUES ('%1s', %2d, %3d );";
	private static final String SELECT_BYPLAYERANDSESSION = "SELECT Spieler, SUM(Wert) AS Wert FROM Sessions WHERE Spieler = '%1s' AND Sessionnumber = %2d;";

	public void erstelleTabelle(SqliteClient client) {
		try {
//			SqliteClient client = new SqliteClient("UnoDatenbank.sqlite");
			if (client.tableExists("Sessions")) { // wenn die Tabelle bereits existiert, dann l�schen
				client.executeStatement("DROP TABLE Sessions;");
			}
			client.executeStatement(CREATETABLE); // erstelle Tabelle

		} catch (SQLException ex) {
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
	}

	public void Insert(String Spieler, int i, int sessionnumber, SqliteClient client) {

		try {

			client.executeStatement(String.format(INSERT_TEMPLATE, Spieler, sessionnumber, i));

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	
	public ArrayList<HashMap<String, String>> ZwischenstandsAbfrage(SqliteClient client) throws SQLException{
		

		return client.executeQuery(SELECT_BYPLAYERANDSESSION);

		
	}

}
