package uno.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import uno.shared.GameReadyMessage;
import uno.shared.LoginMessage;
import uno.shared.ReadyMessage;

public class Client {

	public static void main(String[] args) {

		try (

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~Server-Socket �ffnen und verbinden~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

				Socket client = new Socket("localhost", 4000);
				ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			) {

//~~~~~~~~~~~~~~~~~~~~~~~~~~Spielstart und Login am Server~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			menue();

			System.out.println("Bitte geben Sie Ihren Namen ein:");
			String name = br.readLine();
			oos.writeObject(new LoginMessage(name));
			oos.flush();
			
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~Server bereit?~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
			ReadyMessage readyMessage = (ReadyMessage) ois.readObject();
			System.out.println(readyMessage.getMessage());
			
			if (!readyMessage.isReady()) {
				return;
			}
			
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~Alle Mitspieler da?~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
			System.out.println("Warte auf Mitspieler...");
			GameReadyMessage gameReadyMessage = (GameReadyMessage) ois.readObject();
			
			System.out.println("Wer spielt heute mit?");
			
			for (String player : gameReadyMessage.getOpponents()) {
				System.out.printf(" * [ %s ]\n", player);
			}
			


		} catch (UnknownHostException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void menue() {

		String uno = " .----------------.  .-----------------. .----------------. \r\n"
				+ "| .--------------. || .--------------. || .--------------. |\r\n"
				+ "| | _____  _____ | || | ____  _____  | || |     ____     | |\r\n"
				+ "| ||_   _||_   _|| || ||_   \\|_   _| | || |   .'    `.   | |\r\n"
				+ "| |  | |    | |  | || |  |   \\ | |   | || |  /  .--.  \\  | |\r\n"
				+ "| |  | '    ' |  | || |  | |\\ \\| |   | || |  | |    | |  | |\r\n"
				+ "| |   \\ `--' /   | || | _| |_\\   |_  | || |  \\  `--'  /  | |\r\n"
				+ "| |    `.__.'    | || ||_____|\\____| | || |   `.____.'   | |\r\n"
				+ "| |              | || |              | || |              | |\r\n"
				+ "| '--------------' || '--------------' || '--------------' |\r\n"
				+ " '----------------'  '----------------'  '----------------' ";

		String hello = "\t\n	       Hallo und Herzlich Willkommen bei \t\n" + uno
				+ "\n\n     	       erstellt von Monika und Martina";
		hello += "\n__________________________________________________________________";
		System.out.println(hello);
		System.out.println();
	}
}