package uno.client;

import java.util.ArrayList;
import java.util.Scanner;

import uno.server.Karte;
import uno.server.Spielleiter;

public class Spieler {

	private ArrayList<Karte> Blatt;
	private String name;
	private int Kartenwert;

	public Spieler() {

	}

	public ArrayList<Karte> getBlatt() {
		return Blatt;
	}

	public void setBlatt(ArrayList<Karte> blatt) {
		Blatt = blatt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void aufnehmen(Karte k) {
	}

	public Karte kartenauswählen() {
		return null;
	}

	public ArrayList<Karte> neuesBlattohneKarte(Karte k) {
		return null;
	}

	public void Blattprint() {
	}

	public Boolean getUno(ArrayList<Karte> Handkarten) {
		return null;
	}

	public Boolean getUno(Scanner s1, ArrayList<Karte> Handkarten, Spieler aktuellerSpieler, Spielleiter game) {
		return null;
	}

	public Boolean ziehen() {
		return false;
	}

	public int AktuellerKartenwert() {
		return 0;
	}

	public int getKartenwert() {
		return Kartenwert;
	}

	public void setKartenwert(int kartenwert) {
		Kartenwert = kartenwert;
	}
}
