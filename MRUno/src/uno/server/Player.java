package uno.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import uno.shared.LoginMessage;

public class Player implements Runnable {

	private Socket client;
	private Spielleiter leiter;
	private String name;

	public Player(Socket client, Spielleiter leiter, String name) {
		this.client = client;
		this.leiter = leiter;
	}

	public void run() {

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Spieler-Login verarbeiten~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		try (
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
			) {

			LoginMessage loginMessage = (LoginMessage) ois.readObject();
			name = loginMessage.getName();
			leiter.getGameReady().wait();

		} catch (InterruptedException e) {
			e.printStackTrace();

		} catch (IOException e1) {
			e1.printStackTrace();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public String getName() {
		return name;
	}
}
