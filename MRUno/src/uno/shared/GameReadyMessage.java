package uno.shared;

import java.io.Serializable;

public class GameReadyMessage implements Serializable {

	private static final long serialVersionUID = -6324716742048324318L;
	private String[] opponents;

	public GameReadyMessage(String[] opponents) {
		this.opponents = opponents;
	}

	public String[] getOpponents() {
		return opponents;
	}
}
