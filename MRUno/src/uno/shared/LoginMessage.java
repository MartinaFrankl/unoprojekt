package uno.shared;

import java.io.Serializable;

public class LoginMessage implements Serializable {

	private static final long serialVersionUID = 6214497385899241250L;
	private String name;

	public LoginMessage(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
