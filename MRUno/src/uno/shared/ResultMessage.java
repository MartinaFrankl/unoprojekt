package uno.shared;

import java.io.Serializable;
import java.util.Map;

public class ResultMessage implements Serializable {

	private static final long serialVersionUID = -7951824016227789543L;
	private boolean continueGame;
	private Map<String, Integer> ranking;

	public ResultMessage(boolean continueGame, String answer, Map<String, Integer> ranking) {

		this.continueGame = continueGame;
		this.ranking = ranking;
	}

	public boolean isContinueGame() {
		return continueGame;
	}

	public Map<String, Integer> getRanking() {
		return ranking;
	}

}
