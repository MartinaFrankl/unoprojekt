package uno.shared;

import java.io.Serializable;

public class ReadyMessage implements Serializable {

	private static final long serialVersionUID = -2437711937045057993L;
	private boolean ready;
	private String message;

	public ReadyMessage(boolean ready, String message) {
		this.ready = ready;
		this.message = message;
	}

	public boolean isReady() {
		return ready;
	}

	public String getMessage() {
		return message;
	}
}
